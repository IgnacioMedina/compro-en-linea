$(document).ready(function(){
	
	var form = $('.form_consulta');

	$('.consulta_whatsapp_btn_desktop').click(function(e){
		e.preventDefault();

		var nombre = $('.consulta_nombre').val();
		var apellido = $('.consulta_apellido').val();
		var consulta = $('.consulta_texto_desktop').val();

		var publicacion = location.href;

		var url = "https://api.whatsapp.com/send?phone=543874898080&text=Hola, mi nombre es " + nombre + " " + apellido + ". Tengo una consulta por esta publicación: " + publicacion + ". Consulta: " + consulta;

		$("<a>").attr("href", url).attr("target", "_blank")[0].click();
	});


	$('.consulta_whatsapp_btn_movil').click(function(e){
		e.preventDefault();

		var nombre = $('.consulta_nombre').val();
		var apellido = $('.consulta_apellido').val();
		var consulta = $('.consulta_texto_movil').val();

		var publicacion = location.href;

		var url = "https://api.whatsapp.com/send?phone=543874898080&text=Hola, mi nombre es " + nombre + " " + apellido + ". Tengo una consulta por esta publicación: " + publicacion + ". Consulta: " + consulta;

		$("<a>").attr("href", url).attr("target", "_blank")[0].click();
	});

	form.on('submit', function (e) {
		e.preventDefault();
		$.ajax({
			url: form.attr('action'),
			data: form.serialize(),
			method: form.attr('method'),
			cache: false,
			dataType: 'json',
			success: function( data ) {
				if(data.success == true) {
					form.fadeOut(function(){
						$('.form_consulta_enviada').fadeIn();
					});
					
				}
			},
			complete: function (jqXHR) {
				var csrfToken = jqXHR.getResponseHeader('X-CSRF-Token');
				if (csrfToken) {
					try {
						csrfToken = $.parseJSON(csrfToken);
						var csrfTokenKeys = Object.keys(csrfToken);
						var hiddenFields = srb_form.find('input.csrf[type="hidden"]');
						if (csrfTokenKeys.length === hiddenFields.length) {
							hiddenFields.each(function(i) {
								$(this).attr('name', csrfTokenKeys[i]);
								$(this).val(csrfToken[csrfTokenKeys[i]]);
							});
						}
					}
					catch (e) {

					}
				}
			}
		});
	});

});