<?php

namespace App\Models\Stock;
use Illuminate\Database\Eloquent\Model;

class ProductosImagenes extends Model
{
	protected $table = 'productos_imagenes';

	protected $fillable = [
		'id_producto',
		'descripcion',		
		'enlace',
	];
}
