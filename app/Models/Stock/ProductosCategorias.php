<?php

namespace App\Models\Stock;
use Illuminate\Database\Eloquent\Model;

class ProductosCategorias extends Model
{
	protected $table = 'productos_categoria';

	protected $fillable = [
		'nombre',
	];
}
