<?php

namespace App\Models\Stock;
use Illuminate\Database\Eloquent\Model;
use App\Models\Stock\ProductosImagenes;

class Productos extends Model
{
	protected $table = 'productos';

	protected $fillable = [
		'nombre',
		'precio',		
		'existencias',
		'vistas',
		'sizes',
		'colores',
		'tags',
		'categoria',
		'genero',
		'publicado',
		'publicado_descripcion',
		'disponibilidad',
		'material',
		'talles',
		'etiqueta',
		'etiqueta_descuento'
	];

	public function imagenes() {
		return $this->hasMany('App\Models\Stock\ProductosImagenes','id_producto');
	}

	public function scopeimagen_principal() {
		return ProductosImagenes::where('id_producto', $this->id)->orderBy('id', 'asc')->first();
	}
}
