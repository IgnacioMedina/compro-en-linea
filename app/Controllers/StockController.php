<?php

namespace App\Controllers;


use Slim\Views\Twig as View;
use App\Models\Stock\Productos;
use App\Models\Stock\ProductosCategorias;
use App\Models\Stock\ProductosImagenes;
use App\Models\Stock\Portadas;

use Respect\Validation\Validator as v;

class StockController extends Controller 
{
	public function index($request, $response, array $args)
	{		
		$productos = Productos::orderBy('created_at', 'desc')->get();
		return $this->container->view->render($response, 'admin_views/stock/index.twig', [
			'productos'=>$productos,
		]);
	}

	public function agregar($request, $response, array $args)
	{		
		$categorias = ProductosCategorias::all();
		return $this->container->view->render($response, 'admin_views/stock/agregar.twig', [
			'categorias'=>$categorias,
		]);
	}

	public function borrar($request,$response,$args){
		$id_producto = $args['id'];

		Productos::where('id',$id_producto)->delete();

		$this->flash->addMessage('secondary', 'Publicación eliminada exitosamente.');
		$url = $this->router->pathFor('stock.index');
		return $response->withStatus(302)->withHeader('Location', $url);
	}

	public function agregarpost($request,$response)
	{
		$validation = $this->validator->validate($request, [
			'car_nombre'=>v::notEmpty()->alpha(),
			'car_apellido'=>v::notEmpty()->numeric(),
			'pub_material'=>v::notEmpty()->alpha(),
		]);

		if($validation->failed()) 
		{
			$this->flash->addMessage('error', "No completó todos los campos correctamente, revise de vuelta los datos.");
			$url = $this->router->pathFor('stock.agregar');
			return $response->withStatus(302)->withHeader('Location', $url);
		}


		if($request->getParam('pub_talles') != NULL) { $sizes_array = $request->getParam('pub_talles'); $sizes = implode(",", $sizes_array);  } else { $sizes = "NO"; }
		if($request->getParam('pub_colores') != NULL) { $colores_array = $request->getParam('pub_colores'); $colores = implode(",", $colores_array);  } else { $colores = "NO"; }

		$producto = Productos::Create([
			'nombre'				=>$request->getParam('car_nombre'),
			'precio'				=>$request->getParam('car_apellido'),
			'publicado'				=>$request->getParam('pub_estado'),
			'publicado_descripcion'	=>$request->getParam('pub_contenido'),
			'tags'					=>$request->getParam('pub_keywords'),
			'disponibilidad'		=>$request->getParam('pub_disponibilidad'),
			'material'				=>$request->getParam('pub_material'),
			'etiqueta'				=>$request->getParam('pub_etiqueta_id'),
			'etiqueta_descuento'	=>$request->getParam('pub_etiqueta_descuento'),
			'categoria'				=>$request->getParam('empleado-crear-sucursal'),
			'genero'				=>$request->getParam('empleado-crear-puesto'),
			'talles'				=>$sizes,
			'colores'				=>$colores,
		]);

		$this->flash->addMessage('info', "El producto fue creado exitosamente, puedes editar el mismo para agregarle fotos.");

		$url = $this->router->pathFor('stock.index');
		return $response->withStatus(302)->withHeader('Location', $url);
	}

	public function getfotos($request, $response, array $args)
	{		
		return $this->container->view->render($response, 'admin_views/stock/fotos.twig', [
			'producto'=> Productos::find($args['id']),
			'producto_id'=>$args['id']
		]);
	}

	public function geteditar($request, $response, array $args)
	{		
		$categorias = ProductosCategorias::all();
		return $this->container->view->render($response, 'admin_views/stock/editar.twig', [
			'categorias'=>$categorias,
			'producto'=>Productos::find($args['id']),
		]);
	}

	public function posteditar($request,$response, $args)
	{
		$validation = $this->validator->validate($request, [
			'car_nombre'=>v::notEmpty()->alpha(),
			'car_apellido'=>v::notEmpty()->numeric(),
			'pub_material'=>v::notEmpty()->alpha(),
		]);

		if($validation->failed()) 
		{
			$this->flash->addMessage('error', "No completó todos los campos correctamente, revise de vuelta los datos.");
			$url = $this->router->pathFor('stock.agregar');
			return $response->withStatus(302)->withHeader('Location', $url);
		}


		if($request->getParam('pub_talles') != NULL) { $sizes_array = $request->getParam('pub_talles'); $sizes = implode(",", $sizes_array);  } else { $sizes = "NO"; }
		if($request->getParam('pub_colores') != NULL) { $colores_array = $request->getParam('pub_colores'); $colores = implode(",", $colores_array);  } else { $colores = "NO"; }

		$producto = Productos::find($args['id']);

		$producto->nombre = 			$request->getParam('car_nombre');
		$producto->precio = 			$request->getParam('car_apellido');
		$producto->publicado = 			$request->getParam('pub_estado');
		$producto->publicado_descripcion= $request->getParam('pub_contenido');
		$producto->tags 	= 			$request->getParam('pub_keywords');
		$producto->disponibilidad = 	$request->getParam('pub_disponibilidad');
		$producto->material = 			$request->getParam('pub_material');
		$producto->etiqueta = 			$request->getParam('pub_etiqueta_id');
		$producto->etiqueta_descuento = $request->getParam('pub_etiqueta_descuento');
		$producto->categoria = 			$request->getParam('empleado-crear-sucursal');
		$producto->genero = 			$request->getParam('empleado-crear-puesto');
		$producto->talles = 			$sizes;
		$producto->colores =			$colores;
		$producto->save();

		$this->flash->addMessage('info', "El producto fue editado exitosamente!". $args['id']);

		$url = $this->router->pathFor('stock.index');
		return $response->withStatus(302)->withHeader('Location', $url);
	}



	public function categorias($request, $response, array $args)
	{		
		$categorias = ProductosCategorias::all();
		return $this->container->view->render($response, 'admin_views/stock/categorias.twig', [
			'categorias'=>$categorias,
		]);
	}

	public function categorias_borrar($request,$response,$args){
		$id_categoria = $args['id'];

		ProductosCategorias::where('id',$id_categoria)->delete();

		$this->flash->addMessage('secondary', 'Categoria eliminada exitosamente.');
		$url = $this->router->pathFor('categorias.index');
		return $response->withStatus(302)->withHeader('Location', $url);
	}

	public function categorias_crear($request, $response, array $args)
	{		
		return $this->container->view->render($response, 'admin_views/stock/categorias_crear.twig', [
		]);
	}

	public function categorias_editar($request, $response, array $args)
	{		
		return $this->container->view->render($response, 'admin_views/stock/categorias_editar.twig', [
			'categoria'=>ProductosCategorias::find($args['id']),
		]);
	}

	public function categorias_editarpost($request,$response, $args)
	{
		$validation = $this->validator->validate($request, [
			'nombre_categoria'=>v::notEmpty()->alpha(),
		]);

		if($validation->failed()) 
		{
			$this->flash->addMessage('error', "No completó todos los campos correctamente, revise de vuelta los datos.");
			$url = $this->router->pathFor('categorias.editar', [ 'id' => $args['id']]);
			return $response->withStatus(302)->withHeader('Location', $url);
		}

		$categoria = ProductosCategorias::find($args['id']);
		$categoria->nombre =    $request->getParam('nombre_categoria');
		$portada->save();

		$this->flash->addMessage('info', "La categoría fue editada exitosamente.");

		$url = $this->router->pathFor('categorias.index');
		return $response->withStatus(302)->withHeader('Location', $url);
	}

	public function categorias_crearpost($request,$response)
	{
		$validation = $this->validator->validate($request, [
			'nombre_categoria'=>v::notEmpty()->alpha(),
		]);

		if($validation->failed()) 
		{
			$this->flash->addMessage('error', "No completó todos los campos correctamente, revise de vuelta los datos.");
			$url = $this->router->pathFor('categorias.index');
			return $response->withStatus(302)->withHeader('Location', $url);
		}

		$producto = ProductosCategorias::Create([
			'nombre'				=>$request->getParam('nombre_categoria'),
		]);

		$this->flash->addMessage('info', "La categoría fue creada exitosamente.");

		$url = $this->router->pathFor('categorias.index');
		return $response->withStatus(302)->withHeader('Location', $url);
	}

	public function categoria_cambiarfoto($request, $response, array $args)
	{
		$categoria = ProductosCategorias::find($args['id']);
		$categoria->imagen = $args['foto'];
		$categoria->save();
		return true;
	}

	// -----------------------------------------------------------------------------------
	
	public function portadas($request, $response, array $args)
	{		
		$portadas = Portadas::all();
		return $this->container->view->render($response, 'admin_views/stock/portadas.twig', [
			'portadas'=>$portadas,
		]);
	}


	public function portadas_crear($request, $response, array $args)
	{
		return $this->container->view->render($response, 'admin_views/stock/portadas_crear.twig', [
		]);
	}

	public function portadas_crear_post($request,$response, $args)
	{
		$validation = $this->validator->validate($request, [
			'titulo_portada'=>v::notEmpty(),
			'descripcion_portada'=>v::notEmpty(),
			'boton_portada'=>v::notEmpty(),
			'link_portada'=>v::notEmpty(),
		]);

		if($validation->failed()) 
		{
			$this->flash->addMessage('error', "No completó todos los campos correctamente, revise de vuelta los datos.");
			$url = $this->router->pathFor('portada.crear');
			return $response->withStatus(302)->withHeader('Location', $url);
		}

		$portada = Portadas::Create([
		'titulo' =>			$request->getParam('titulo_portada'),
		'descripcion' =>	$request->getParam('descripcion_portada'),
		'boton' =>			$request->getParam('boton_portada'),
		'alineacion' =>		$request->getParam('alineacion_portada'),
		'link' =>			$request->getParam('link_portada'),
		]);

		$this->flash->addMessage('info', "La portada fue creada exitosamente! ID:". $portada->id );

		$url = $this->router->pathFor('portadas.index');
		return $response->withStatus(302)->withHeader('Location', $url);
	}


	public function portadas_editar($request, $response, array $args)
	{
		return $this->container->view->render($response, 'admin_views/stock/portadas_editar.twig', [
			'portada'=>Portadas::find($args['id']),
		]);
	}

	public function portadas_editar_post($request,$response, $args)
	{
		$validation = $this->validator->validate($request, [
			'titulo_portada'=>v::notEmpty(),
			'descripcion_portada'=>v::notEmpty(),
			'boton_portada'=>v::notEmpty(),
			'link_portada'=>v::notEmpty(),
		]);

		if($validation->failed()) 
		{
			$this->flash->addMessage('error', "No completó todos los campos correctamente, revise de vuelta los datos.");
			$url = $this->router->pathFor('portada.editar', [ 'id' => $args['id']]);
			return $response->withStatus(302)->withHeader('Location', $url);
		}

		$portada = Portadas::find($args['id']);

		$portada->titulo = 			$request->getParam('titulo_portada');
		$portada->descripcion = 	$request->getParam('descripcion_portada');
		$portada->boton = 			$request->getParam('boton_portada');
		$portada->alineacion = 		$request->getParam('alineacion_portada');
		$portada->link = 			$request->getParam('link_portada');
		$portada->save();

		$this->flash->addMessage('info', "La portada fue editada exitosamente! ID:". $args['id'] );

		$url = $this->router->pathFor('portadas.index');
		return $response->withStatus(302)->withHeader('Location', $url);
	}

	public function portada_cambiarfoto($request, $response, array $args)
	{
		$portada = Portadas::find($args['id']);
		$portada->imagen = $args['foto'];
		$portada->save();
		return true;
	}

	public function borrar_foto($request, $response, array $args)
	{
		$id_foto = $args['id'];
		ProductosImagenes::where('id', '=', $id_foto)->delete();

		$this->flash->addMessage('info', "La foto fue eliminada correctamente!");
		$url = $this->router->pathFor('photoupload', [ 'id' => $args['producto'] ]);
		return $response->withStatus(302)->withHeader('Location', $url);		
	}


	// -----------------------------------------------------------------------------------

	public function postPhotoUploader($request, $response, $args) {
		
		$id_producto = $args['id'];

		$error = false;
		$err_desc = "";
		$files = array();

		$uploaddir = './images/uploads/';
		$factory = new \RandomLib\Factory;
		$generator = $factory->getMediumStrengthGenerator();
		$uuid = $generator->generateString(64,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
		$a = 1;
		foreach($_FILES as $file)
		{
			$extension = pathinfo(parse_url($file['name'])['path'], PATHINFO_EXTENSION);

			$nuevo_nombre = $uuid.'-'.$a.'.jpg';

			$fichero = $uploaddir.basename($nuevo_nombre);
			while(file_exists($fichero)) 
			{
				$uuid = $generator->generateString(64,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
				$nuevo_nombre = $uuid.'-'.$a.'.jpg';
				$fichero = $uploaddir.basename($nuevo_nombre);
			}
			
			$moved = move_uploaded_file($file['tmp_name'], $uploaddir.basename($nuevo_nombre));		
			if($moved)
			{
				$this->compress_image($uploaddir.basename($nuevo_nombre), $uploaddir.basename($nuevo_nombre), 80);
				$files[] = $uploaddir .$nuevo_nombre;
			}
			else
			{
				$error = true;
				$err_desc = $file['error'];
			}
			$a++;
		}
		$data = ($error) ? array('success' => false, 'error_desc'=>$err_desc) : array('success' => true, 'files' => $files);

		if($data['success'] == true) 
		{
			$archivos = $data['files'];
			$orden = ProductosImagenes::where('id_producto',$id_producto)->get()->count();
			if(is_array($archivos)) 
			{
				foreach ($archivos as $key=>$value) {
					$orden = $orden + 1;
					$archivo = str_replace('./', '/', $value);
					ProductosImagenes::Create([
						'id_producto'=>$id_producto,
						'enlace'=>$archivo,
					]);
				}
			}
			else 
			{
				$archivo = str_replace('./', '/', $archivos);
				ProductosImagenes::Create([
					'id_producto'=>$id_producto,
					'enlace'=>$archivo,
				]);
			} 
			$this->flash->addMessage('info', 'Todas las fotos han sido cargadas exitosamente.');
		} 
		else 
		{
			$this->flash->addMessage('error', 'Ha ocurrido un problema.');
		}

		echo json_encode(array("success"=>true));
	}

	public function compress_image($source_url, $destination_url, $quality){

		$info = getimagesize($source_url);
		ini_set('gd.jpeg_ignore_warning', 1);
		if ($info['mime'] == 'image/jpeg') {
			$image = @imagecreatefromjpeg($source_url);
			if(!$image) {
				$image = imagecreatefromstring(file_get_contents($source_url));
			}			
		}
		

		elseif ($info['mime'] == 'image/gif')
		$image = imagecreatefromgif($source_url);

		elseif ($info['mime'] == 'image/png')
		$image = imagecreatefrompng($source_url);

		imagejpeg($image, $destination_url, $quality);
		return $destination_url;

	}

}