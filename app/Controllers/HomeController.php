<?php

namespace App\Controllers;


use Slim\Views\Twig as View;
use App\Models\Publicacion;
use App\Models\Consulta;
use App\Models\Suscripcion;
use App\Models\Individuo;
use App\Models\Redireccion;

use Respect\Validation\Validator as v;
use App\Auth\auth;

use App\Models\Stock\Productos;
use App\Models\Stock\ProductosCategorias;
use App\Models\Stock\Portadas;

class HomeController extends Controller 
{
	public function notfound($request, $response, array $args)
	{
		return $this->container->view->render($response->withStatus(404), 'guest_views/vehiculos/404.twig',[]);
	}


	public function index($request, $response, array $args)
	{		
		return $this->container->view->render($response, 'guest_views/home_fixed.twig', [
			'categorias'=>ProductosCategorias::all(),
			'productos'=>Productos::orderByRaw('RAND()')->take(6)->get(),
			'productos2'=>Productos::orderByRaw('RAND()')->skip(6)->take(6)->get(),
			'portadas'=>Portadas::all(),
			'destacados'=>Productos::where('disponibilidad','=',1)->orderBy('vistas','DESC')->limit(2)->get(),
		]);
	}


	public function index_productos($request, $response, array $args)
	{		
		return $this->container->view->render($response, 'guest_views/home_productos.twig', [
			'categorias'=>ProductosCategorias::all(),
			'productos'=>Productos::all(),
			'portadas'=>Portadas::all(),
			'destacados'=>Productos::where('disponibilidad','=',1)->orderBy('vistas','DESC')->limit(2)->get(),
		]);
	}

	public function vercategoria($request, $response, array $args)
	{		
		$categoria_ = ProductosCategorias::where('nombre','like',$args['titulo'])->first();
		$categoria = $categoria_->id;
		return $this->container->view->render($response, 'guest_views/home_categoria.twig', [
			'categorias'=>ProductosCategorias::all(),
			'categoria'=>ProductosCategorias::find($categoria),
			'productos'=>Productos::where('categoria','=', $categoria)->get(),
			'destacados'=>Productos::where('disponibilidad','=',1)->orderBy('vistas','DESC')->limit(2)->get(),
		]);
	}

	public function postConsulta($request, $response) {
		if($request->isXhr()){

			/*if($this->container->auth->check()) {
				$usuario = $this->container->auth->user();
				if($usuario->individuo->telefono == NULL) {
					$usuario->individuo->telefono = $request->getParam('consulta_telefono');
					$usuario->individuo->save();
				}				
			}*/

			$consulta = Consulta::create([
				'id_usuario'=>$request->getParam('consulta_userid'),
				'nombre'=>$request->getParam('consulta_nombre'),
				'apellido'=>$request->getParam('consulta_apellido'),
				'texto'=>$request->getParam('consulta_texto'),
				'telefono'=>$request->getParam('consulta_telefono'),
				'url'=>$request->getParam('consulta_url'),
				'email'=>$request->getParam('consulta_email'),
			]);

			return $response->withJson([
				'success'=>true,
			]);
		}

		return $response->withJson([
			'success'=>true,
		]);
	}

	public function postBusqueda($request, $response) {
		if($request->isXhr()) {
			
			$marcas = Marca::orderBy('nombre', 'ASC')->get();
			$vehiculos = Vehiculo::orderBy('vistas', 'desc')->get();

			$input = $request->getParam('sbrq');

			$keywords = array();
			foreach($marcas as $marca) {
				$nombre_marca = $marca->nombre;
				if($marca->tienevehiculos()) {
					array_push($keywords, array('t'=>0,'marca'=>$nombre_marca));	
				}
			}			
			foreach($vehiculos as $kv=>$v) {
				if(!$v->EstaPublicado()) { unset($vehiculos[$kv]); continue; }				
				array_push($keywords, array('t'=>1,'marca'=>$v->getMarca->nombre, 'modelo'=>$v->modelo, 'year'=>$v->year));
			}

			$resultados = array();
			$sfs = new \wataridori\SFS\SimpleFuzzySearch($keywords, ['marca', 'modelo', 'year'], $input);
			$tmp_res = $sfs->search();			
			if(is_array($tmp_res)) {
				// El primer resultado del array es siempre el más específico.

				foreach($tmp_res as $sres) {
					$marca = $sres[0]['marca'];

					$tmp_marca = r_sinacentos($marca);
					$tmp_marca = strtolower($tmp_marca);
					$tmp_marca = str_replace(' ', '-', $tmp_marca);
					$url_modelo = $tmp_marca;

					$url_marca = strtolower($marca);

					$nr = array('marca'=>$marca);
					$url_params = 'marca/'.$url_marca;


					if(isset($sres[0]['modelo'])) {
						$modelo = $sres[0]['modelo'];
						$tmp_modelo = r_sinacentos($modelo);
						$tmp_modelo = strtolower($tmp_modelo);
						$tmp_modelo = str_replace(' ', '-', $tmp_modelo);
						$url_modelo = $tmp_modelo;

						$nr['modelo'] = ' ' . $modelo;
						$url_params.="/modelo/".$url_modelo;
					} else { $nr['modelo'] = ''; }

					if(isset($sres[0]['year'])) {
						$year = $sres[0]['year'];



						$nr['year'] = $year . ' ';
						$url_params.="/year/".$year;
					} else { $nr['year'] = ''; }

					$nr['url'] = $this->container->router->pathFor('vehicle.filter', ['params'=>$url_params]);


					/*if(isset($sres[0]['modelo'])) {
						
						$modelo = $sres[0]['modelo'];
						$tmp_modelo = r_sinacentos($modelo);
						$tmp_modelo = strtolower($tmp_modelo);
						$tmp_modelo = str_replace(' ', '-', $tmp_modelo);
						$url_modelo = $tmp_modelo;

						$url = $this->container->router->pathFor('vehicle.filter', ['params'=>'marca/'.$url_marca.'/modelo/'.$url_modelo]);
						$nr = array('marca'=>$marca, 'modelo'=>' ' . $modelo, 'url'=>$url);
					}
					else {
						$url = $this->container->router->pathFor('vehicle.filter', ['params'=>'marca/'.$url_marca]);
						$nr = array('marca'=>$marca, 'modelo'=>'', 'url'=>$url);
					}*/
					$resultados[] = $nr;
				}
			}

			return $response->withJson([
				'success'=>true,
				'res'=>$resultados,
			]);
		}
	}

	public function postTest($request, $response, $args) {

		if ($request->isXhr()) {

			$individuo = Individuo::where('nombre', 'like', '%'.$request->getParam('nombre').'%')->get();
			return $response->withJson(['success' => true, 'individuos'=>$individuo, 'request'=>$request->getParams()]);
		} else {
			return $response->withRedirect('/');
		}
	}

	public function verarticulo($request, $response, array $args){

		$productos = Productos::find($args['id']);
		$categoria = ProductosCategorias::find($productos->categoria);
		$colores = explode(',', $productos->colores);
		$talles = explode(',', $productos->talles);

		$productos->vistas = $productos->vistas + 1;
		$productos->save();

		return $this->container->view->render($response, 'guest_views/ver.twig',[
			'categoria'=>$categoria,
			'producto'=> $productos,
			'colores'=> $colores,
			'talles'=> $talles,
			'destacados'=>Productos::where('disponibilidad','=',1)->orderBy('vistas','DESC')->limit(4)->get(),
		]);
	}

	/*******************************/
	public function filtrarvehiculos($request, $response, array $args){
		$parametros = str_replace(array('/marca/', 'marca/', '/marca'), '#!#marca#@#', $args['params']);
		$parametros = str_replace(array('/ubicacion/', 'ubicacion/', '/ubicacion'), '#!#ubicacion#@#', $parametros);
		$parametros = str_replace(array('/year/', 'year/', '/year'), '#!#year#@#', $parametros);
		$parametros = str_replace(array('/transmision/', 'transmision/', '/transmision'), '#!#transmision#@#', $parametros);
		$parametros = str_replace(array('/motor/', 'motor/', '/motor'), '#!#motor#@#', $parametros);
		$parametros = str_replace(array('/modelo/', 'modelo/', '/modelo'), '#!#modelo#@#', $parametros);

		$tags = explode('#!#', $parametros);
		foreach($tags as $tag) {
			$tmp_tag = explode('#@#', $tag);
			if(isset($tmp_tag[1])) {		
				$filtred_tags[$tmp_tag[0]] = str_replace('-', ' ', $tmp_tag[1]);
			}
		}

		$unset = 0;
		$filter_warning_message = null;
		$filter_info_message = null;

		$search_keys = array();
		foreach($filtred_tags as $fkey=>$tag) {
			switch($fkey) {
				case 'ubicacion': {
					$tmp = Localidad::where('nombre', 'like', '%'.$tag.'%')->first();
					$search_for = 'id_localidad'; $return = 'id'; $show = 'nombre';
					break;
				}
				case 'marca': {
					$tmp = Marca::where('nombre', 'like', '%'.$tag.'%')->first();
					$search_for = 'id_marca';	$return = 'id';	$show = 'nombre';
					break;
				}
				case 'transmision': {
					$tmp = Transmision::where('nombre', 'like', '%'.$tag.'%')->first();
					$search_for = 'id_transmision'; $return = 'id'; $show = 'nombre';
					break;					
				}
				case 'motor': {
					$tmp = TiposMotor::where('nombre', 'like', '%'.$tag.'%')->first();
					$search_for = 'id_tipo_motor'; $return = 'id'; $show = 'nombre';
					break;
				}
				case 'modelo': {
					$tmp = Vehiculo::where('modelo', 'like', '%'.$tag.'%')->orderBy('modelo')->first();
					$search_for = 'modelo'; $return = 'modelo'; $show = 'modelo';
					break;
				}
				case 'year': {
					$tmp = Vehiculo::where('year', 'like', '%'.$tag.'%')->first();
					$search_for = 'year'; $return = 'year'; $show = 'year';
					break;
				}
			}
			if($tmp) {				
				$tmp->search_for = $search_for;
				$tmp->return = $return;
				$tmp->show = $tmp->$show;
				$filtred_tags[$fkey] = $tmp;
				$search_keys[$search_for] = $tmp;			
			}
			else {
				$unset++;
			}
		}
		if($unset > 0) {
			$filter_warning_message = 'Algunos filtros no son válidos.';
		} else {
			$filter_info_message = 'Se aplicaron los filtros correctamente.';
		}

		$publicaciones = Publicacion::where('mostrar', 1)->get();

		$real_publicaciones = array();
		foreach ($publicaciones as $publicacion) {
			$vehiculo = $publicacion->vehiculo;
			$filter = true;
			foreach($search_keys as $skey=>$sk){
				$ret = $sk->return;
				if($vehiculo->$skey != $sk->$ret) { $filter = false; }
			}
			if($filter) {
				$real_publicaciones[] = $publicacion;
			}
		}


		$modelos = array();
		foreach($real_publicaciones as $publicacion) {
			$v = $publicacion->vehiculo;
			if(!array_key_exists($v->modelo, $modelos)) {
				$modelos[$v->modelo] = 1;
			}
			else {
				$modelos[$v->modelo]++;	
			}
		}

		$years = array();
		foreach($real_publicaciones as $publicacion) {
			$v = $publicacion->vehiculo;
			if(!array_key_exists($v->year, $years)) {
				$years[$v->year] = 1;
			}
			else {
				$years[$v->year]++;	
			}
		}
		$paginaMarcas = Marca::with('paginas')->whereHas('paginas',function($query){
			$query->where('mostrar',1);
		})->get();
		if(count($real_publicaciones)==1){
			return $response->withRedirect($this->router->pathFor('vehicle.ver',[
					'id' => $real_publicaciones[0]->id,
					'titulo' => str_replace(" ","_",strtolower(trim($real_publicaciones[0]->titulo))),
					'paginasMarca'=> $paginaMarcas,
				])
			);
		} else {
			return $this->container->view->render($response, 'guest_views/vehiculos/filtrar.twig',[
				'publicaciones'=>$real_publicaciones,
				'marcas'=>Marca::orderBy('nombre', 'asc')->get(),
				'localidades'=>Localidad::orderBy('nombre', 'asc')->get(),
				'transmisiones'=>Transmision::orderBy('nombre', 'asc')->get(),
				'motores'=>TiposMotor::orderBy('nombre', 'asc')->get(),
				'years'=>$years,
				'modelos'=>$modelos,
				'filtros'=>$filtred_tags,
				'filter_warning_message'=>$filter_warning_message,
				'filter_info_message'=>$filter_info_message,
				'search_keys'=>$search_keys,
				'paginas'=>Paginas::where('mostrar',1)->orderBy('id','DESC')->get(),
				'sugerencia_sin_fotos'=> ['<p style="margin-top: 3rem;">Vehiculo sin imagen.<br> Por el momento.</p>'],
				'paginasMarca'=> $paginaMarcas,
				//'sugerencia_sin_fotos' => ['Imagenes Ya!','Esperando la vista.','Insertar Imagen Aqui.','Muy Pronto las Fotos.','Subiendo Imagenes.','Construyendo las fotos','Sin Fotos! Aun...','... ya vienen las Fotos.','Cargando Graficos.'],
			]);
		}
		

	}


	public function getFAQS($request, $response) {
		return $this->container->view->render($response, 'guest_views/faqs/faqs.twig',[
		]);
	}

	public function getNosotros($request, $response) {
		$paginaMarcas = Marca::with('paginas')->whereHas('paginas',function($query){
			$query->where('mostrar',1);
		})->get();
		return $this->container->view->render($response, 'guest_views/nosotros/nosotros.twig',[
			'paginas'=>Paginas::where('mostrar',1)->orderBy('id','DESC')->get(),
			'paginasMarca'=> $paginaMarcas,
		]);
	}

	public function getContacto($request, $response) {
		return $this->container->view->render($response, 'guest_views/contacto/contacto.twig',[
		]);
	}

	public function getVendeTuAuto($request, $response) {
		$paginaMarcas = Marca::with('paginas')->whereHas('paginas',function($query){
			$query->where('mostrar',1);
		})->get();
		if($this->container->auth->check()){
			$id_usuario = auth::individuo()->id_usuario;
			$vehiculos = Vehiculo::leftJoin('usados_terceros','vehiculos.id','=','usados_terceros.id_vehiculo')->where('eliminado',0)->where('id_usuario',$id_usuario)->get();
			return $this->container->view->render($response, 'guest_views/vehiculos/vender.twig', [
				'marcas'=>Marca::orderBy('nombre', 'asc')->get(),
				'paginas'=>Paginas::where('mostrar',1)->orderBy('id','DESC')->get(),
				'localidades'=>Localidad::orderBy('nombre', 'asc')->get(),
				'transmisiones'=>Transmision::orderBy('nombre', 'asc')->get(),
				'motores'=>TiposMotor::orderBy('nombre', 'asc')->get(),
				'provincias'=>Provincia::orderBy('nombre', 'asc')->get(),
				'ubicaciones'=>Ubicacion::orderBy('nombre', 'asc')->get(),
				'estados'=>EstadosVeh::orderBy('nombre', 'asc')->get(),
				'paginasMarca'=> $paginaMarcas,
				'vehiculos' => $vehiculos,
			]);
		} else {
			$this->flash->addMessage('info', 'Para publicar tu vehículo, es necesario que inicies sesión.');
			return $response->withStatus(301)->withHeader('Location', $this->router->pathFor('auth.signin',['return_to'=>'vendetuauto']));
		}

	}

	public function compress_image($source_url, $destination_url, $quality)
	{

		$info = getimagesize($source_url);
		ini_set('gd.jpeg_ignore_warning', 1);
		if ($info['mime'] == 'image/jpeg') {
			$image = @imagecreatefromjpeg($source_url);
			if(!$image) {
				$image = imagecreatefromstring(file_get_contents($source_url));
			}			
		}
		

		elseif ($info['mime'] == 'image/gif')
		$image = imagecreatefromgif($source_url);

		elseif ($info['mime'] == 'image/png')
		$image = imagecreatefrompng($source_url);

		imagejpeg($image, $destination_url, $quality);
		return $destination_url;

	}

	public function postRecibirInformacionVehiculo($request, $response) {
		if($request->isXhr()){
		
			$vehiculo = Vehiculo::Create([
				'id_marca'=>$request->getParam('form_sell_car_info_marca'),
				'modelo'=>$request->getParam('form_sell_car_info_modelo'),
				'year'=>$request->getParam('form_sell_car_info_year'),
				'motor'=>$request->getParam('form_sell_car_info_motor'),
				'id_tipo_motor'=>$request->getParam('form_sell_car_info_tipomotor'),
				'id_transmision'=>$request->getParam('form_sell_car_info_transmision'),
				'cantidad_puertas'=>$request->getParam('form_sell_car_info_cantidadpuertas'),
				'id_localidad'=>$request->getParam('form_sell_car_info_localidad'),
				'id_ubicacion'=>-1,
				'entrega_minima'=>-1,
				'precio_venta'=>$request->getParam('form_sell_car_info_precioventa'),
				'estado_vehiculo'=>3,
				'id_usuario' => auth::individuo()->id_usuario,
			]);

			$dominio_vehiculo = str_replace(array(' ', '-'), '', trim($request->getParam('form_sell_car_info_dominio')));

			$usado = UsadoTercero::Create([
				'dominio'=>$dominio_vehiculo,
				'id_vehiculo'=>$vehiculo->id,
				'kilometraje'=>$request->getParam('form_sell_car_info_kilometraje'),
				'id_owner'=>$_SESSION['userid'],
				'observaciones'=>$request->getParam('form_sell_car_info_observaciones'),
				'color'=>$request->getParam('form_sell_car_info_color'),
			]);


			$error = false;
			$err_desc = "";
			$files = array();

			$uploaddir = './images/uploads/';
			$factory = new \RandomLib\Factory;
			$generator = $factory->getMediumStrengthGenerator();
			$uuid = $generator->generateString(64,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
			$a = 1;
			foreach($_FILES as $file)
			{
				$extension = pathinfo(parse_url($file['name'])['path'], PATHINFO_EXTENSION);

				$nuevo_nombre = $uuid.'-'.$a.'.jpg';

				$fichero = $uploaddir.basename($nuevo_nombre);
				while(file_exists($fichero)) {
					$uuid = $generator->generateString(64,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
					$nuevo_nombre = $uuid.'-'.$a.'.jpg';
					$fichero = $uploaddir.basename($nuevo_nombre);
				}
				
				$moved = move_uploaded_file($file['tmp_name'], $uploaddir.basename($nuevo_nombre));		
				if($moved)
				{
					$this->compress_image($uploaddir.basename($nuevo_nombre), $uploaddir.basename($nuevo_nombre), 80);
					$files[] = $uploaddir .$nuevo_nombre;
				}
				else
				{
					$error = true;
					$err_desc = $file['error'];
				}
				$a++;
			}
			$data = ($error) ? array('success' => false, 'error_desc'=>$err_desc) : array('success' => true, 'files' => $files);

			if($data['success'] == true) {
				$archivos = $data['files'];
				$orden = ImagenesVeh::where('id_vehiculo',$id_vehiculo)->get()->count();
				if(is_array($archivos)) {
					foreach ($archivos as $key=>$value) {
						$orden = $orden + 1;
						$archivo = str_replace('./', '/', $value);
						ImagenesVeh::Create([
							'id_vehiculo'=>$vehiculo->id,
							'archivo'=>$archivo,
							'orden'=>$orden,
							'id_usuario'=>$_SESSION['userid']
						]);
					}
				}
				else {
					$archivo = str_replace('./', '/', $archivos);
					ImagenesVeh::Create([
						'id_vehiculo'=>$vehiculo->id,
						'archivo'=>$archivo,
						'orden'=>$orden+1,
						'id_usuario'=>$_SESSION['userid']
					]);
				}

				return $response->withJson([
					'success'=>true
				]);
			}		
		}

		return $response->withJson([
			'success'=>false,
		]);
	}

	public function get0km($request,$response){
		$paginaMarcas = Marca::with('paginas')->whereHas('paginas',function($query){
			$query->where('mostrar',1);
		})->get();

		return $response->withStatus(200)->withJson($paginaMarcas);
	}

	public function eliminarTercero($request,$response,$args){
		$id_vehiculo = $args['id'];
		$id_usuario = auth::individuo()->id_usuario;
		$vehiculo = Vehiculo::where('eliminado',0)->where('id_usuario',$id_usuario)->where('id',$id_vehiculo)->get();
		if($vehiculo){
			Vehiculo::where('id',$id_vehiculo)->update([
				'eliminado' => 1,
			]);

			Publicacion::where('id_vehiculo', $id_vehiculo)
			->where('mostrar',1)
			->update(['mostrar' => 0]);

			VehiculoHistorial::Create([
				'id_vehiculo' => $args['id'],
				'id_usuario' => $_SESSION['userid'],
				'descripcion' => 'Vehiculo Tercero Eliminado',
				'id_estado' => 4,
			]);

			$this->flash->addMessage('info', 'Vehiculo Eliminado con Exito. La publicacion asosiada tambien fue dado de baja, si es que esta en curso.');
		} else {
			$this->flash->addMessage('info', 'Vehiculo no encontrado.');
		}
		return $response->withRedirect($this->router->pathFor('vendetuauto'));
	}

	
}