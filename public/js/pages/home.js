$(document).ready(function() {

	var srb_form = $('.sbr_form');
	sessionStorage.setItem('sul', 'null');
	srb_form.on('submit', function () {
		
		return false;
	});

	$('.searchbartxt').on('focus', function(){
		console.log(sessionStorage.getItem('sul'));
		if(sessionStorage.getItem('sul') != 'null') {
			$('#'+sessionStorage.getItem('sul')).fadeIn(300).addClass('opened');
		}
	});

	$( ".searchbartxt" ).autocomplete({
		open: function () {
			$( ".searchbartxt" ).addClass('searching');
			$('ul.ui-autocomplete').addClass('opened');
		},
		close: function () {
			$( ".searchbartxt" ).removeClass('searching');
			$('ul.ui-autocomplete').removeClass('opened'); 
		},
		focus: function( event, ui ) {
      $( ".searchbartxt" ).val( ui.item.year + ui.item.marca + ui.item.modelo );
      return false;
    },
    select: function( event, ui ) {
      $( ".searchbartxt" ).val( ui.item.year + ui.item.marca + ui.item.modelo );
     	location.href = ui.item.url;
      return false;
    },
		source: function(request, response) {
			$.ajax({
				url: srb_form.attr('action'),
				data: srb_form.serialize(),
				method: srb_form.attr('method'),
				cache: false,
				dataType: 'json',
				success: function( data ) {
					response( data.res);
					console.log(data);
				},
				complete: function (jqXHR) {
					var csrfToken = jqXHR.getResponseHeader('X-CSRF-Token');
					if (csrfToken) {
						try {
							csrfToken = $.parseJSON(csrfToken);
							var csrfTokenKeys = Object.keys(csrfToken);
							var hiddenFields = srb_form.find('input.csrf[type="hidden"]');
							if (csrfTokenKeys.length === hiddenFields.length) {
								hiddenFields.each(function(i) {
									$(this).attr('name', csrfTokenKeys[i]);
									$(this).val(csrfToken[csrfTokenKeys[i]]);
								});
							}
						}
						catch (e) {

						}
					}
				}
			});
		},
		minLength: 2
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
		sessionStorage.setItem('sul', ul.attr('id'));
		console.log(item);
		var str = item.year + item.marca + item.modelo;
		var t = String(str).replace(
			new RegExp(this.term, "gi"),
			"<span class='srb-highlight'>$&</span>");
		return $("<li>").data("item.autocomplete", item)
			.append("<div class='srb-item' data-marca='"+item.marca+"' data-year='"+item.year+"' data-modelo='"+item.modelo+"'>" + t + "</div>")
			.appendTo(ul);
	};
});