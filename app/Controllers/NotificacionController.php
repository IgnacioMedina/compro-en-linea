<?php

namespace App\Controllers;

use Slim\Views\Twig as View;
use App\Models\Notificaciones\Notificaciones;

class NotificacionController extends Controller {
	public function enviar($request, $response)
	{
		return $this->container->view->render($response, 'admin_views/notificaciones/enviar.twig', [
		]);
	}

	public function index($request, $response)
	{
		return $this->container->view->render($response, 'admin_views/notificaciones/index.twig', [ 
			'notificaciones_totales' => Notificaciones::where('id_usuario', $_SESSION['userid'])->orderBy('created_at', 'DESC')->get(),
		]);
	}

	public function ver($request, $response, array $args)
	{
		$notificacion = Notificaciones::find($args['id']);
		$notificacion->estado = 1;
		$notificacion->save();
		return true;
	}

	public function cargar($request, $response, array $args)
	{
		 /* canales:
			| 1-> notificacion web
			| 2-> worker chrome
			| 3-> worker app
			| 4-> correo electronico
			| 5-> correo electronico alternativo
		|*/

		// notificación web -> se crea con la SQL
		Notificaciones::Create([
			'id_usuario' => $args['id'],
			'prioridad' => $args['prioridad'],
			'categoria' => $args['cat'],
			'mensaje' =>$args['mensaje']
		]);

		// worker chrome:
				
		return true;
	}

	public function postEnviar($request, $response, array $args)
	{
		 /* canales:
			| 1-> notificacion web
			| 2-> worker chrome
			| 3-> worker app
			| 4-> correo electronico
			| 5-> correo electronico alternativo
		|*/

		// notificación web -> se crea con la SQL
		Notificaciones::Create([
			'id_usuario' => $args['id'],
			'prioridad' => $args['prioridad'],
			'categoria' => $args['cat'],
			'mensaje' =>$args['mensaje']
		]);

		// worker chrome:
				
		return true;
	}
}