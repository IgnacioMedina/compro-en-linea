<?php
use App\Middleware\AuthMiddleware;
use App\Middleware\GuestMiddleware;
use App\Middleware\AdminMiddleware;
use App\Middleware\NuevoMiddle;
use App\Middleware\SessionMiddleware;

use Aurmil\Slim\CsrfTokenToView;
use Aurmil\Slim\CsrfTokenToHeaders;


$app->get('/', 'HomeController:index')->setName('home');
$app->get('/inicio', 'HomeController:index')->setName('inicio');
$app->get('/{titulo}_{id:[0-9]+}', 'HomeController:verarticulo')->setName('articulo.ver');
$app->get('/categoria/{titulo}/', 'HomeController:vercategoria')->setName('categoria.ver');
$app->get('/productos', 'HomeController:index_productos')->setName('productos');

$app->post('/buscarpublicacion', 'HomeController:postBusqueda')->setName('publicaciones.buscar')->add(new CsrfTokenToHeaders($container->csrf));
$app->get('/preguntasfrecuentes', 'HomeController:getFAQS')->setName('faqs');
$app->get('/contacto', 'HomeController:getContacto')->setName('contacto');
$app->post('/consulta', 'HomeController:postConsulta')->setName('consulta')->add(new CsrfTokenToHeaders($container->csrf));
$app->post('/suscripcion', 'SuscripcionController:post')->setName('suscripcion');

$app->group('', function() {
	$this->get('/registro[/{return_to:.*}]', 'AuthController:getSignUp')->setName('auth.signup');
	$this->post('/registro', 'AuthController:postSignUp');

	$this->get('/iniciar-sesion[/{return_to:.*}]', 'AuthController:getSignIn')->setName('auth.signin');
	$this->post('/iniciar-sesion', 'AuthController:postSignIn');
	$this->post('/asociar-facebook', 'AuthController:asociar-facebook')->setName('asociar.facebook');

	$this->post('/facebook-login', 'AuthController:postFacebookRequest')->setName('auth.facebook');

})->add(new GuestMiddleware($container));

$app->group('', function() {
	$this->get('/bienvenido', 'AuthController:FirstWelcome')->setName('auth.firstwelcome');
	$this->get('/cerrar-sesion', 'AuthController:getSignOut')->setName('auth.signout');
	
})->add(new AuthMiddleware($container))->add(new SessionMiddleware($container));

$app->group('/administracion', function() {
	$this->get('[/]', 'AdminController:index')->setName('admin.index');


	$this->group('/empleados', function() {
		$this->get('[/listado-empleados]', 'EmpleadosController:index')->setName('empleados.index');
		$this->get('/cargar-empleado', 'EmpleadosController:getCargar')->setName('empleados.cargar');
		$this->get('/editar-empleado/empid_{id:[0-9]+}', 'EmpleadosController:getEditar')->setName('empleados.editar');
		$this->get('/eliminar-empleado/empid_{id:[0-9]+}', 'EmpleadosController:getEliminar')->setName('empleados.eliminar');
		$this->post('/cargar-empleado', 'EmpleadosController:postCrear');
		$this->post('/editar-empleado', 'EmpleadosController:postEditar')->setName('empleados.editarpost');

		$this->get('/correos', 'VirtualController:index')->setName('correovirtual.index');
		$this->get('/{id:[0-9]+}/correos', 'VirtualController:agregarCorreo')->setName('empleados.correo.agregar');
		$this->post('/correos', 'VirtualController:agregarUsuarioVirtual')->setName('correovirtual.agregar');
		$this->get('/{id:[0-9]+}/correos/eliminar', 'VirtualController:eliminarUsuarioVirtual')->setName('correovirtual.eliminar');
		$this->get('/{id:[0-9]+}/correos/habilitar', 'VirtualController:habilitarUsuarioVirtual')->setName('correovirtual.habilitar');
		$this->post('/correos/alias', 'VirtualController:agregarAlias')->setName('correovirtual.alias');
		$this->get('/correos/{id:[0-9]+}/alias/eliminar', 'VirtualController:eliminarAlias')->setName('correovirtual.alias.eliminar');

	});

	$this->group('/godmode', function() {
		$this->get('[/]', 'ModulosController:index')->setName('godmode.index');

		$this->group('/modulo',function(){
			$this->get('[/listado-modulos]', 'ModulosController:index')->setName('modulos.index');
			$this->post('/crear','ModulosController:crearModulo')											      	->setName('godmode.modulo.crear');
			$this->post('/{id:[0-9]+}/editar','ModulosController:editarModulo')								->setName('godmode.modulo.editar');
			$this->post('/{id:[0-9]+}/eliminar','ModulosController:eliminarModulo')						->setName('godmode.modulo.eliminar');

		});
		$this->group('/enlace',function(){
			$this->post('/crear','ModulosController:crearEnlace')											      	->setName('godmode.enlace.crear');
			$this->post('/{id:[0-9]+}/editar','ModulosController:editarEnlace')								->setName('godmode.enlace.editar');
			$this->post('/{id:[0-9]+}/eliminar','ModulosController:eliminarEnlace')						->setName('godmode.enlace.eliminar');

		});
	});

	$this->group('/notificaciones', function() {
		$this->get('[/]', 'NotificacionController:index')->setName('notificaciones.index');
		$this->post('/enviar', 'NotificacionController:postEnviar')->setName('notificaciones.enviar');
		$this->get('/visto/{id:[0-9]+}', 'NotificacionController:ver')->setName('notificaciones.visto');
		$this->get('/cargar/{id:[0-9]+}/{prioridad:[0-9]+}/{cat:[0-9]+}/{mensaje}', 'NotificacionController:cargar')->setName('notificaciones.cargar');
	});


	$this->group('/perfil', function() {
		$this->get('[/]', 'PerfilController:index')->setName('perfil.index');
		$this->get('/cambiarfoto/{id:[0-9]+}/{foto}', 'PerfilController:cambiarfoto')->setName('perfil.cambiarfoto');
		$this->post('/contrasenia', 'PerfilController:cambiarContraseña')->setName('perfil.contraseña');
		$this->post('/datos', 'PerfilController:cambiarDatos')->setName('perfil.datos');
		
	});

	$this->group('/productos', function() {
		$this->get('[/]', 'StockController:index')->setName('stock.index');
		$this->get('/agregar', 'StockController:agregar')->setName('stock.agregar');
		$this->get('/{id:[0-9]+}/borrar', 'StockController:borrar')->setName('stock.borrar');
		$this->get('/{id:[0-9]+}/editar', 'StockController:geteditar')->setName('stock.editar');
		$this->post('/{id:[0-9]+}/editar', 'StockController:posteditar')->setName('stock.editar.post');
		$this->post('/agregar', 'StockController:agregarpost')->setName('stock.agregar.post');
		$this->get('/{id:[0-9]+}/subir-fotos', 'StockController:getfotos')->setName('photoupload');
		$this->post('/{id:[0-9]+}/subir-fotos', 'StockController:postPhotoUploader');
		$this->get('/eliminar-foto/{producto:[0-9]+}/{id:[0-9]+}', 'StockController:borrar_foto')->setName('borrar.foto');
	});

	$this->group('/categorias', function() {
		$this->get('[/]', 'StockController:categorias')->setName('categorias.index');
		$this->get('/crear', 'StockController:categorias_crear')->setName('categorias.crear');
		$this->post('/crear', 'StockController:categorias_crearpost')->setName('categorias.crear.post');
		$this->get('/editar/{id:[0-9]+}', 'StockController:categorias_editar')->setName('categorias.editar');
		$this->post('/editar/{id:[0-9]+}', 'StockController:categorias_editar_post')->setName('categorias.editar.post');
		$this->get('/borrar/{id:[0-9]+}', 'StockController:categorias_borrar')->setName('categorias.borrar');

		$this->get('/cambiarfoto/{id:[0-9]+}/{foto}', 'StockController:categoria_cambiarfoto')->setName('categorias.cambiarfoto');
	});

	$this->group('/portadas', function() {
		$this->get('[/]', 'StockController:portadas')->setName('portadas.index');
		$this->get('/editar/{id:[0-9]+}', 'StockController:portadas_editar')->setName('portada.editar');
		$this->post('/editar/{id:[0-9]+}', 'StockController:portadas_editar_post')->setName('portada.editar.post');
		$this->get('/crear', 'StockController:portadas_crear')->setName('portada.crear');
		$this->post('/crear', 'StockController:portadas_crear_post')->setName('portada.crear.post');
		
		$this->get('/cambiarfoto/{id:[0-9]+}/{foto}', 'StockController:portada_cambiarfoto')->setName('portada.cambiarfoto');
	});

	$this->group('/ayuda', function() {
		$this->get('[/]', 'AyudaController:index')->setName('ayuda.index');
		$this->get('/{id:[0-9]+}/', 'AyudaController:verManual')->setName('ayuda.ver');
		$this->get('/crear', 'AyudaController:crearManual')->setName('ayuda.crear');
		$this->get('/editar/{id:[0-9]+}', 'AyudaController:editarManual')->setName('ayuda.editar');
		$this->post('/editar/{id:[0-9]+}', 'AyudaController:editarManualPost')->setName('ayuda.editarpost');
		$this->get('/eliminar/{id:[0-9]+}', 'AyudaController:eliminarManual')->setName('ayuda.eliminar');
		$this->post('/crear', 'AyudaController:crearManualPost')->setName('ayuda.crearpost');
	});

})->add(new AdminMiddleware($container))->add(new SessionMiddleware($container));

?>