<?php

namespace App\Middleware;
use App\Models\Permisos;
use App\Models\ModuloEnlaces;


class AdminMiddleware extends Middleware
{
	public function __invoke($request, $response, $next)
	{	


		if(!$this->container->auth->check())
		{
			$this->container->flash->addMessage('error', 'Debes iniciar sesión para realizar esta acción.');
			return $response->withRedirect($this->container->router->pathFor('auth.signin'));
		}

		if($this->container->auth->empleado()->acceso_sistema <= 1)
		{
			$this->container->flash->addMessage('error', 'No tienes autorización para ingresar a esta área.');
			return $response->withRedirect($this->container->router->pathFor('home'));
		}

		$route = $request->getAttribute('route');
		$ruta_actual = $route ? $route->getName() : null;
		if($ruta_actual != 'admin.index' && $ruta_actual != null) {
			$permisos_enlaces = Permisos::where('id_usuario', $_SESSION['userid'])->get();

			$acceso = false;
			foreach($permisos_enlaces as $pe)
			{
				$data_enlace = ModuloEnlaces::find($pe->id_enlace);	
				if($data_enlace->url_name == $ruta_actual) 
				{
					$acceso = true;
				}
				$acceso = true;
			}
			if($acceso == false) {

				$sin_acceso = ModuloEnlaces::where('url_name', $ruta_actual)->first();

				$this->container->flash->addMessage('error', 'No tienes acceso a ' . $sin_acceso->nombre .'' );
				return $response->withRedirect($this->container->router->pathFor('admin.index'));
			}
		}

		$response = $next($request, $response);
		return $response;
	}
}

?>