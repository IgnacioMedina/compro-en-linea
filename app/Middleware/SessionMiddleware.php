<?php

namespace App\Middleware;

use App\Models\UserSession;

class SessionMiddleware extends Middleware
{
	public function __invoke($request, $response, $next)
	{
		$session = UserSession::where('id',$_SESSION['id_session'])->first();
		if($session->estado == 0){
			$this->container->flash->addMessage('error', 'Probablemente tiene otra session en proseso. Vuelva a ingresar.');
			$this->container->auth->logout();
			return $response->withStatus(301)->withRedirect($this->container->router->pathFor('auth.signin'));
		}
		
		$response = $next($request, $response);
		return $response;
	}
}