<?php

namespace App\Controllers;

use Slim\Views\Twig as View;
use App\Models\Stock\Productos;
use App\Models\Stock\ProductosCategorias;
use App\Models\Stock\ProductosImagenes;
use App\Models\Stock\Portadas;

use Illuminate\Database\Capsule\Manager as DB;

class AdminController extends Controller {

	public function index($request, $response)
	{
		$cantidad_consultas = 1;

		return $this->container->view->render($response, 'admin_views/home.twig',[
			'consultas'=>$cantidad_consultas,
			'cantidad_productos'=>Productos::all()->count(),
			'vistas_hoy'=>Productos::all()->count(),
			'vistas_totales'=>Productos::all()->count(),
		]);
	}
}