<?php

namespace App\Models\Notificaciones;
use Illuminate\Database\Eloquent\Model;

class Notificaciones extends Model
{
	protected $table = 'notificacines_categoria';

	protected $fillable = [
		'nombre_categoria',		
		'color',
	];
}