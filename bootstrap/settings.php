<?php
return [
    'settings' => [
        'determineRouteBeforeAppMiddleware' => true,
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
        'db' => [
            'driver'=>'mysql',
            'host'=>'localhost',
            'database'=>'comproen_v2',
            'username'=>'root',
            'password'=>'Y5LHi2pwAe4f',
            'charset'=>'utf8',
            'collation'=>'utf8_unicode_ci',
            'prefix'=>'',
            ],
        'logger' => [
      'name' => 'ciro',
      'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
      'level' => \Monolog\Logger::DEBUG,
    ],
    ]   
];