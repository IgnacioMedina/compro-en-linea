
	$(document).ready(function() {
		sessionStorage.setItem('nowpp', 'false');

		setTimeout(function(){
			$('.flashmessage').slideUp();
		}, 3000);

		$('[data-toggle="popover-whatsapp"]').popover({
			container: 'body',
			trigger: 'hover focus',
			placement: 'left',
			title: 'Obtén más información sobre este vehículo', 
			content: "",
			html: true,			
			template: '<div class="popover popover-whatsapp text-white" role="tooltip"><div class="arrow arrow-wpp"></div><h3 class="popover-title text-white">Obtén más información sobre este vehículo</h3><div class="popover-content">Hacé clic y comunicate instantáneamente por WhatsApp con uno de nuestros asesores. <button class="btn btn-sm btn-flat btn-block d-lg-none nowpp" id="nocapo">No gracias.</button></div></div>'
		}).on('hide.bs.popover', function () {
			$(this).data('hidden', false);
		  $(this).data('changing', true);
		}).on('hidden.bs.popover', function () {
		  $(this).data('changing', false);
		  $(this).data('hidden', true);
		}).on('shown.bs.popover', function () {
		  $(this).data('changing', false);
		  $(this).data('hidden', false);
		}).on('show.bs.popover', function () {
		  $(this).data('changing', true);
		  $(this).data('hidden', false);
		});
		$('[data-toggle="popover-link"]').popover({
			container: 'body',
			trigger: 'hover focus',
			placement: 'left',
			title: 'Copiar el enlace para compartirlo',
			content: "",
			html: true,			
			template: '<div class="popover popover-link" role="tooltip"><div class="arrow"></div><h3 class="popover-title">Copiar el enlace para compartirlo</h3></div>'
		});

		$.fn.scrollEnd = function(callback, timeout) {			
		  $(this).scroll(function(){
		  	$('[data-toggle="popover-whatsapp"]').each(function(){
		  		if($(this).data('changing') == true) { return true;	}
		  		$(this).popover('hide');
		  	});
		    var $this = $(this);
		    if ($this.data('scrollTimeout')) {		    	
		      clearTimeout($this.data('scrollTimeout'));
		    }
		    $this.data('scrollTimeout', setTimeout(callback,timeout));
		  });
		};
		$(document).click('.nowpp', function(){
			sessionStorage.setItem('nowpp', 'true');
		});
		$(window).scrollEnd(function(){
			if($('#grid').data('columns') != '1' || sessionStorage.getItem('nowpp') == 'true') {return false;}
			$('.vehicle-card').each(function(){
				var vcard = $(this);
				if($(this).visible(false, true)) {
					$(this).find('[data-toggle="popover-whatsapp"]').popover('show');
				}
			});
		}, 1000);
	});