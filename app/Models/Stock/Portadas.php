<?php

namespace App\Models\Stock;
use Illuminate\Database\Eloquent\Model;

class Portadas extends Model
{
	protected $table = 'portadas';

	protected $fillable = [
		'titulo',
		'descripcion',		
		'boton',
		'imagen',
		'alineacion',
		'link',
	];
}
